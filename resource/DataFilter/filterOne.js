const data = require("../data.js");
const errorHandler = require("../errorHandler.js");

function filterOne (data) {
// Tempat penampungan hasil
const result = [];

data.forEach((data) => {
    if (data.age < 30 && data.favoriteFruit === "banana") {
        result.push(data);
    }
    
})

return (result.length < 1) ? errorHandler() : result;
}


module.exports = filterOne(data)