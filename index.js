// Import express module
const express = require('express')
const app = express()
const port = 3000

// Import express-ejs-layout
const expressLayouts = require('express-ejs-layouts')

const fs = require("fs");
let rawData = fs.readFileSync("./resource/data.json");
const parsing = JSON.parse(rawData);

// Static files untuk menampilkan css dan image
app.use(express.static('public'))

// Setting up menggunakan ejs
app.set('view engine', 'ejs')
app.use(expressLayouts)

// Import
const dataSet = require("./resource/data.js")
const filterOne = require("./resource/DataFilter/filterOne.js");
const filterTwo = require('./resource/DataFilter/filterTwo.js');
const filterThree = require('./resource/DataFilter/filterThree.js');
const filterFour = require('./resource/DataFilter/filterFour.js');
const filterFive = require('./resource/DataFilter/filterFive.js');

const {filter} = require('./resource/filter.js');


app.get('/', (req, res) => {
  res.render('index', {
    layout: 'layouts/main-layout',
    title: 'Home Page'
  })
})

app.get('/about', (req, res) => {
  res.render('about', {
    layout: 'layouts/main-layout',
    title: 'About Page'
  })
})

// app.get('/data', (req, res) => {
//   res.render('tableData',{
//     data: dataSet,
//     title: 'All data',
//     layout: 'layouts/main-layout',
//   })
// })

// request query untuk search data
app.get("/data", (req, res) => {
  const search = req.query.company?.toLowerCase()
  let data = filter(parsing, search);
  res.render('tableData', {
    title: "Data page",
    data: data,
    layout: 'layouts/main-layout',
  });
});


app.get("/data/1", (req, res) => {
  res.render('tableData', {
    title: "Data Pertama",
    data: filterOne,
    layout: 'layouts/main-layout',
  });
});

app.get("/data/2", (req, res) => {
  res.render('tableData', {
    title: "Data Pertama",
    data: filterTwo,
    layout: 'layouts/main-layout',
  });
});

app.get("/data/3", (req, res) => {
  res.render('tableData', {
    title: "Data Pertama",
    data: filterThree,
    layout: 'layouts/main-layout',
  });
});

app.get("/data/4", (req, res) => {
  res.render('tableData', {
    title: "Data Pertama",
    data: filterFour,
    layout: 'layouts/main-layout',
  });
});

app.get("/data/5", (req, res) => {
  res.render('tableData', {
    title: "Data Pertama",
    data: filterFive,
    layout: 'layouts/main-layout',
  });
});

// Dengan req query dan req param untuk favourite fruit dan minimal age
// app.get('/data/:fruit', (req,res) => {
//   let data = filter(parsing, req.params.fruit,req.query)
//   res.render("tableData", {
//     title: "Data Buah dan umur",
//     data,
//     layout: 'layouts/main-layout',
//   })
// })

app.use('/', (req, res) => {
  res.status(404)
  res.render('404', {
    layout: 'layouts/main-layout',
    title: 'Not found',
  })
})

app.listen(port, () => {
  console.log(`Sever sudah berjalan di http://localhost:${port}`)
})

